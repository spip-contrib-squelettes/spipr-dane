<?php
$GLOBALS['oembed_providers'] = array(
    'http://webtv.restauration.ac-versailles.fr/*' => 'http://webtv.ac-versailles.fr/oembed.api',
    'https://www.audio-lingua.eu/*' => 'https://www.audio-lingua.eu/oembed.api',
    'https://pod.ac-caen.fr/video*' => 'https://pod.ac-caen.fr/oembed/',
    'https://pod.ac-normandie.fr/video*' => 'https://pod.ac-normandie.fr/oembed/',
    'https://learningapps.org/*'=> 'https://learningapps.org/oembed.php',
    'https://podeduc.apps.education.fr/video*'=>'https://podeduc.apps.education.fr/oembed/',
);

define('_SURLIGNE_RECHERCHE_REFERERS',true);

